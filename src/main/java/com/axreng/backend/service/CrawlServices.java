package com.axreng.backend.service;

import com.axreng.backend.model.CrawlRequest;
import com.axreng.backend.model.ErrorResponse;
import com.axreng.backend.model.ResultResponse;
import com.axreng.backend.model.CrawlResponse;
import com.axreng.backend.util.IdGenerator;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import spark.Request;
import spark.Response;

public class CrawlServices {

    private static final List<ResultResponse> results = new ArrayList<>();
    private static final Gson gson = new Gson();
    private static final String REGEX = "<a\\s+[^>]*?href=[\"']?([^\"'\\s>]+)[\"']?[^>]*?>";
    private static final String CONTENT_TYPE = "application/json";
	private static final String REQUEST_BODY_MISSING_OR_EMPTY_MESSAGE = "Request body missing or empty";
	private static final String INVALID_REQUEST_BODY_MESSAGE = "Invalid request body";
	private static final String INVALID_KEYWORD_LENGTH_MESSAGE = "Invalid keyword length";
	private static final String SEARCH_NOT_FOUND_MESSAGE = "Search not found";
    private static final String ID_PARAMETER_MISSING_MESSAGE = "Parameter ':id' is required";
	private static final String ACTIVE_STATUS = "active";
	private static final String DONE_STATUS = "done";
	private static final String BASE_URL = System.getenv("BASE_URL");
    private static final Integer MAX_STACK_SIZE  = 10000;
    private static final Integer MAX_LIST_SIZE = 150; // Maximum limit of 150 results
    private static final Integer MIN_KEYWORD_SIZE = 4; // Minimum limit of 4 
    private static final Integer MAX_KEYWORD_SIZE = 32; // Maximum limit of 32 
	
    public String startCrawl(Request req, Response res) {
        // Set Content-Type Response
        res.type(CONTENT_TYPE);

		if (req.body() == null || req.body().isEmpty()) {
            // Return Bad Request
            res.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return gson.toJson(new ErrorResponse(REQUEST_BODY_MISSING_OR_EMPTY_MESSAGE));
		}
		
		try {		
			String requestBody = req.body();
			CrawlRequest request = gson.fromJson(requestBody, CrawlRequest.class);

			// Checks if the keyword length
			if (request.getKeyword().length() < MIN_KEYWORD_SIZE || request.getKeyword().length() > MAX_KEYWORD_SIZE) {
				// Return Bad Request
				res.status(HttpURLConnection.HTTP_BAD_REQUEST);
				return gson.toJson(new ErrorResponse(INVALID_KEYWORD_LENGTH_MESSAGE));
			}

			// Generate the ID
			String idSearch = IdGenerator.generateId();
			
			// Add ongoing search to the list.
			ResultResponse result = new ResultResponse(idSearch, ACTIVE_STATUS, new ArrayList<>());
			results.add(result);

			// Create the Search Thread
			Thread serviceThread = new Thread(() -> {
				int[] stackSize = {0};
				performService(request.getKeyword(), result, stackSize);
				updateResult(idSearch, result.getUrls());
			});
			serviceThread.start();

			// Return Ok
			res.status(HttpURLConnection.HTTP_OK);
			return gson.toJson(new CrawlResponse(idSearch));
		} catch (Exception e) {
			// Return Bad Request
            res.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return gson.toJson(new ErrorResponse(INVALID_REQUEST_BODY_MESSAGE));
		}
    }

    private void performService(String keyword, ResultResponse result, int[] stackSize) {
        try {
            String htmlContent = getHtmlContent(BASE_URL);

            if (htmlContent != null && htmlContent.toLowerCase().contains(keyword.toLowerCase())) {
                result.getUrls().add(BASE_URL);
            }
                
			stackSize[0]++;
			findUrlsWithKeyword(htmlContent, keyword, result.getUrls(), stackSize);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getHtmlContent(String url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setRequestMethod("GET");
		
		StringBuilder content = new StringBuilder();
        
        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            try (
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                
                String line;

                while ((line = reader.readLine()) != null) {
                    content.append(line);
                }
            }
        }

		return content.toString();
    }

    private void findUrlsWithKeyword(String htmlContent, String keyword, List<String> foundUrls, int[] stackSize) {
		Pattern pattern = Pattern.compile(REGEX, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(htmlContent);
		
		while (matcher.find() && stackSize[0] < MAX_STACK_SIZE  && foundUrls.size() < MAX_LIST_SIZE) {
			stackSize[0]++;
			String href = matcher.group(1);
			
			if (href.startsWith(BASE_URL)) {
				try {
					String linkedHtmlContent = getHtmlContent(href);
					
					// Check if the link contains the keyword
					if (linkedHtmlContent != null && linkedHtmlContent.toLowerCase().contains(keyword.toLowerCase())) {
						// Not include repeated url
						if (hasUrl(foundUrls, href) == null ) {
							foundUrls.add(href);
					
							// Recursively continue the search through the links
							findUrlsWithKeyword(linkedHtmlContent, keyword, foundUrls, stackSize);
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (!href.startsWith("http://www.") && !href.startsWith("https://www.")) {
				try {
					String url = BASE_URL+href;
					String linkedHtmlContent = getHtmlContent(url);

					// Check if the link contains the keyword
					if (linkedHtmlContent != null && linkedHtmlContent.toLowerCase().contains(keyword.toLowerCase())) {
						// Not include repeated url
						if (hasUrl(foundUrls, url) == null ) {
							foundUrls.add(url);
							
							// Recursively continue the search through the links
							findUrlsWithKeyword(linkedHtmlContent, keyword, foundUrls, stackSize);
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
    }

    private void updateResult(String idSearch, List<String> foundUrls) {
        ResultResponse result = findResultById(idSearch);

        if (result != null) {
            result.setStatus(DONE_STATUS);
            result.setUrls(foundUrls);
        }
    }

    private String hasUrl(List<String> foundUrls, String newUrl) {
        return foundUrls.stream()
               .filter(url -> url.equals(newUrl))
               .findFirst()
               .orElse(null);
    }

    private ResultResponse findResultById(String idSearch) {
        return results.stream()
                .filter(result -> result.getId().equals(idSearch))
                .findFirst()
                .orElse(null);
    }

    public String getCrawlStatus(Request req, Response res) {
        // Set Content-Type Response
        res.type(CONTENT_TYPE);

		if (req.params(":id") == null || req.params(":id").isEmpty()) {
            // Return Bad Request
            res.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return gson.toJson(new ErrorResponse(ID_PARAMETER_MISSING_MESSAGE));
		}

        String idSearch = req.params(":id");

        ResultResponse result = findResultById(idSearch);

        if (result!= null) {
            // Return Ok
            res.status(HttpURLConnection.HTTP_OK);
            return gson.toJson(result);
        } else {
            // Return Bad Request
            res.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return gson.toJson(new ErrorResponse(SEARCH_NOT_FOUND_MESSAGE));
        }
    }
}