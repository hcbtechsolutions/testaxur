package com.axreng.backend.model;

public class CrawlResponse {

    private String id;

    // It's necessary to Gson
    public CrawlResponse() {}

    public CrawlResponse(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}