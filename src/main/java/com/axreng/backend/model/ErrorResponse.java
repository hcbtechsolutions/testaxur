package com.axreng.backend.model;

public class ErrorResponse {

    private String error;

    // It's necessary to Gson
    public ErrorResponse() {}

    public ErrorResponse(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}