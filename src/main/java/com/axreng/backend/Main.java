package com.axreng.backend;

import com.axreng.backend.service.CrawlServices;
import java.net.HttpURLConnection;
import spark.Spark;
import static spark.Spark.*;

public class Main {

	private static final String ROUTE_NOT_FOUND_MESSAGE = "Route not found. Check the URL and try again.";

    public static void main(String[] args) {
        CrawlServices services = new CrawlServices();
        
        Spark.port(4567);

        get("/crawl/:id", services::getCrawlStatus);
        post("/crawl", services::startCrawl);
        
		// Route for all other requests (GET, etc.)
        path("/crawl/", () -> {
            before("/*", (req, res) -> {
                // Display a message for all other requests
                if (!req.requestMethod().equals("POST")) {
                    res.body(ROUTE_NOT_FOUND_MESSAGE);
                }
            });
        });
    }
} 
